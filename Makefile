# Makefile for the pytogo project
VERSION=$(shell sed <pytogo -n -e '/^version *= *\(.*\)/s//\1/p')

PREFIX=/usr
MANDIR=$(PREFIX)/share/man/man1
BINDIR=$(PREFIX)/bin

DOCS    = README.adoc COPYING control NEWS BUGS pytogo.adoc pytogo.1
SOURCES = pytogo Makefile $(DOCS) tests/ #pytogo-logo.png

all: pytogo-$(VERSION).tar.gz

install: pytogo.1
	cp pytogo $(BINDIR)
	gzip <pytogo.1 >$(MANDIR)/pytogo.1.gz
	rm pytogo.1

pytogo-$(VERSION).tar.gz: $(SOURCES)
	mkdir pytogo-$(VERSION)
	cp -r $(SOURCES) pytogo-$(VERSION)
	tar -czf pytogo-$(VERSION).tar.gz pytogo-$(VERSION)
	rm -fr pytogo-$(VERSION)
	ls -l pytogo-$(VERSION).tar.gz

pytogo-$(VERSION).md5: pytogo-$(VERSION).tar.gz
	@md5sum pytogo-$(VERSION).tar.gz >pytogo-$(VERSION).md5

.SUFFIXES: .html .adoc .txt .1

# Requires asciidoc and xsltproc/docbook stylesheets.
.adoc.1:
	a2x --doctype manpage --format manpage $<
.adoc.html:
	a2x --doctype manpage --format xhtml -D . $<
	rm -f docbook-xsl.css
check:
	@make pylint
	@cd tests >/dev/null; make --quiet

version:
	@echo $(VERSION)

PYLINTOPTS = --rcfile=/dev/null --reports=n
pylint:
	@pylint --ignore=.git $(PYLINTOPTS) pytogo

dist: pytogo-$(VERSION).tar.gz

clean:
	rm -f pytogo.html pytogo.1
	rm -f *.pyc *~ bugs.html
	rm -f index.html *.tar.gz *.md5 *old

release: pytogo-$(VERSION).tar.gz pytogo-$(VERSION).md5 pytogo.html
	shipper version=$(VERSION) | sh -e -x

htmlclean:
	rm *.html
refresh: htmlclean pytogo.html
	shipper -N -w version=$(VERSION) | sh -e -x

