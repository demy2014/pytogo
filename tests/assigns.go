//# Check logic for detecting single assignment
// a should be flagged, b should not
func func1() {
    a := 1
    b = 2
    b = 3
}

// only c should be flagged
func func2() {
    global a
    a = 1
    b = 2
    b = 3
    c := 4
}

// only z should be flagged
func func3() {
    a = 1
    b = 2
    b = 3
    def func4() {
        a = 2
        z := 1
    }
}

// Ensure that a is flagged despite assignments in previous scopes
func func5() {
    a := 0
}
