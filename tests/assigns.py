## Check logic for detecting single assignment
def func1():
    # a should be flagged, b should not
    a = 1
    b = 2
    b = 3

def func2():
    # only c should be flagged
    global a
    a = 1
    b = 2
    b = 3
    c = 4

def func3():
    # only z should be flagged
    a = 1
    b = 2
    b = 3
    def func4():
        a = 2
        z = 1

def func5():
    # Ensure that a is flagged despite assignments in previous scopes
    a = 0
