//# Test class method translation
    class NodeAction(object) {
        __slots__ = ("revision", "path", "kind", "action",
                     "from_rev", "from_path", "content_hash",
                     "from_hash", "blob", "props", "propchange",
                     "from_set", "blobmark", "generated")
        // If these don't match the constants above, havoc will ensue
        ActionValues = ("add", "delete", "change", "replace")
        PathTypeValues = ("none", "file", "dir", "ILLEGAL-TYPE")
        // These are set during parsing
        func (self @@) __init__(revision) {
            self.revision = revision
            self.path = nil
            self.kind = SD_NONE
            self.action = nil
            self.from_rev = nil
            self.from_path = nil
            self.content_hash = nil
            self.from_hash = nil
            self.blob = nil
            self.props = nil
            self.propchange = false
            // These are set during the analysis phase
            self.from_set = nil
            self.blobmark = nil
            self.generated = false
        }
        // Prefer dict's repr() to OrderedDict's verbose one
        func (self @@) String() string {
            fmt := dict.__repr__ if isinstance(self.props, dict) else repr
            return "<NodeAction: r{rev} {action} {kind} '{path}'"
                    "{from_rev}{from_set}{generated}{props}>".format(
                    rev := self.revision,
                    action := "ILLEGAL-ACTION" if self.action == nil else StreamParser.NodeAction.ActionValues[self.action],
                    kind := StreamParser.NodeAction.PathTypeValues[self.kind || -1],
                    path := self.path,
                    from_rev := fmt.Sprintf(" from=%s~%s", self.from_rev, self.from_path)
                                    if self.from_rev else "",
                                    }
                    from_set := fmt.Sprintf(" sources=%s", self.from_set)
                                    if self.from_set else "",
                                    }
                    generated := " generated" if self.generated else "",
                    props := fmt.Sprintf(" properties=%s", fmt)(self.props)
                                    if self.props else "")
                                    }
        }
        __repr__ = __str__
    }
