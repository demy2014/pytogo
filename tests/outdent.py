## Test fix for pathological outdent
    while true:
	line = sp.readline()
	if not line:
	    break
	else if not line.strip():
	    continue
	else if line.startswith(" # reposurgeon-read-options:"):
	    options = options.union(line.split(":")[1].split())
	else if line.startswith("UUID:"):
	    sp.repo.uuid = sdBody(line)
	else if line.startswith("Revision-number: "):
	    # Begin Revision processing
	    announce(debugSVNPARSE, "revision parsing, line %d: begins" % (sp.importLine))

