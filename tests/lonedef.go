//# Test replacement of def at left margin, and imports.

import (
	"calendar"
	"cgi"
	"cmd"
	"codecs"
	"compress/gzip"
	"difflib"
	"fmt"
	"github.com/google/uuid"
	"hashlib"
	"io"
	"io/ioutil"
	"net/mail"
	"os"
	"os/exec"
	"path/filepath"
	"reflect"
	"regexp"
	"shutil"
	"signal"
	"tarfile"
	"time"
)

lastfrac = 0

func readprogress(baton, fp, filesize) {
    if filesize != nil {
        global lastfrac
        frac := (fp.tell() / (filesize * 1.0))
        if frac > lastfrac + 0.01 {
            baton.twirl(fmt.Sprintf("%d%%", frac * 100))
            lastfrac = frac
            i++
        }
    }
    baton.twirl()

}
fmt.Print("Should pull in fmt library")
