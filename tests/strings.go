//# Test single-quote conversion in strings
"foo"       // Should not change
"bar"       // Single quotes should become double
'foo\"bar'  // Only outermost double quotes should change
'hi"there'  // Embedded double quote should prevent change
"foo" "bar" // Second literal should change
"foo" "bar" // First literal should change
