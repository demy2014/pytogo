//# Test return-type detection

// An example docstring.
func predicate(e) bool {
    return true
}

// Another example docstring.
func predicate2(e) int {
    return 0
}

func pep484(a int, b string) string {
    fmt.Print("I see you!")
    return b % a

}
// Python program to illustrate
// *args with first extra argument
func myFun(arg1, argv ...) {
    print ("First argument :", arg1)
    for _, arg := range argv {
        fmt.Print("Next argument through *argv :", arg)
    }
}

