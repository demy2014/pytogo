//# Verify correct processing of hanging indents.
        for revision, record := range self.revisions {
            for _, node := range record.nodes {
                if node.fromPath != nil {
                    copynodes = append(copynodes, node)
                    announce(debugEXTRACT, fmt.Sprintf("copynode at %s", node))
                }
                if node.action == sdADD || node.kind == sdDIR {
                    for _, trial := range global_options["svn_branchify"] {
                        if "*" !in trial || trial == node.path {
                            self.branches[node.path+string(os.PathSeparator)] = nil
                        } else if trial.endswith(string(os.PathSeparator) + "*")
                                 || filepath.Dir(trial)
                                 || node.path + string(os.PathSeparator) + "*" !in options {
                            self.branches[node.path+string(os.PathSeparator)] = nil
                        } else if trial == "*" {
                            self.branches[node.path+string(os.PathSeparator)] = nil
                        }
                    }
                    if node.path+string(os.PathSeparator) in self.branches {
                        announce(debugSHOUT, "recognized as a branch)
                    }
                }
            }
            // Per-commit spinner disabled because this pass is fast
            //baton.twirl("")
        }
        copynodes.sort(key=operator.attrgetter("fromRev"))
